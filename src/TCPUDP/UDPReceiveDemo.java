package TCPUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPReceiveDemo {

	public static void main(String[] args) throws IOException{
		int port = 6000;
		DatagramSocket ds = new DatagramSocket(port);
		while(true){
			byte[] data = new byte[1024];
			DatagramPacket dp = new DatagramPacket(data,data.length);
			ds.receive(dp);
			int len = dp.getLength();
			System.out.print(dp.getAddress().getHostAddress());
			System.out.println(new String(data,0,len));			
		}


		
//		ds.close();
		
		
		
	}

}
