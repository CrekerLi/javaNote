package TCPUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

/*
 * UDP协议发送
 * java.net.DatagramPacket 包装数据
 * java.net.DatagramSocket 发送数据
 */




public class UDPSendDemo {

	public static void main(String[] args) throws IOException{
		Scanner sc = new Scanner(System.in);
		InetAddress inet  =  InetAddress.getByName("192.168.1.81");
		int port = 6000;
		DatagramSocket ds = new DatagramSocket();
		while(true){
			String msg = sc.nextLine();
			byte data[] = msg.getBytes();
			int len=data.length;
			DatagramPacket  p = new DatagramPacket(data,len,inet,port);
			ds.send(p);
			
			
			
		}
//		ds.close();
	}

}
