package tcpClientServer;


import java.io.*;
import java.net.Socket;
import java.net.SocketException;



public class tcpClient {

	public static void main(String[] args) throws IOException{
		// 创建socket对象
		String s = "服务器OK?";
		Socket socket = new Socket("192.168.1.81",6666);
		
		//发送数据
		OutputStream out = (OutputStream) socket.getOutputStream(); 
		out.write(s.getBytes());
		
		//读取服务器发送的数据
		InputStream in = socket.getInputStream();
		byte[] data = new byte[1024];
		int len = in.read(data);
		System.out.println(new String(data,0,len));
		
		
		
		socket.close();

	}

}
 